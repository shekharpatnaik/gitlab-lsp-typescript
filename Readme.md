# GitLab Language Server - Typescript (Experiment)

This is an experiment to provide [GitLab AI Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html) into [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/) enabled IDEs. This is an **experiment** and is subject to the guidelines in our [documentation](https://docs.gitlab.com/ee/policy/alpha-beta-support.html#experiment).

All feedback should be directed to [here](https://gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-language-server/-/issues).

[[_TOC_]]

## Introduction

LSP is a technology that provides an abstraction layer between tools that
provide analysis results (language servers) and the "consumer" IDEs (language
clients). It provides a generic interface to provide analysis results in
LSP-enabled IDEs so that the analysis only has to be implemented once while
multiple IDEs can benefit from the analysis.

This is an LSP-based language-server that serves [GitLab AI Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html) to LSP enabled IDEs.

The communication is implemented according to the documentation laid out in the
[API docs](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/tree/main)

This language server leverages the [textdocument/completion](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_completion) LSP method to serve code suggestion. For most editors this hooks into the intelliSense functionality which can be often invoked by means of the `[Ctrl]+[SPACE]` `[Ctrl]+[N]` shortcuts.

# Running the LSP

Download the binary for your platform and then run

```sh
gitlab-lsp --token=$GITLAB_TOKEN
```

# Development

## Running the LSP on TCP

```sh
export GITLAB_TOKEN="my"
npm run compile && npm start
```

## Packaging the LSP
```sh
npm run package
```