import { IGitLabAPI, GitLabAPI } from "./api";
import { LspServer, Mode } from "./lsp_server";
import yargs from 'yargs/yargs';

function run() {
    const argv = yargs(process.argv.slice(2)).options({
        port: { type: 'number', default: 6789 },
        host: { type: 'string', default: 'https://gitlab.com' },
        token: { type: 'string' },
        mode: { choices: ['tcp', 'stdin'], default: 'tcp' }
    }).demandOption(['token']).parseSync();

    const api: IGitLabAPI = new GitLabAPI(argv.host, argv.token);
    const server = new LspServer(argv.port, api, argv.mode == 'tcp' ? Mode.TCP : Mode.STDIN);
    server.start();
}

run();