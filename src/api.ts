import fetch from 'cross-fetch';

const USER_AGENT = 'code-completions-language-server-experiment';

export interface IGitLabAPI {
    GetCodeSuggestions(file_info: CodeSuggestionRequestCurrentFile): Promise<CodeSuggestionResponse | undefined>;
}

export interface CodeSuggestionRequest {
    prompt_version: number,
    project_path: string,
    project_id: number,
    current_file: CodeSuggestionRequestCurrentFile
}

export interface CodeSuggestionRequestCurrentFile {
    file_name: string,
    content_above_cursor: string,
    content_below_cursor: string
}

export interface CodeSuggestionResponse {
    choices: CodeSuggestionResponseChoice[]
}

export interface CodeSuggestionResponseChoice {
    text: string
}

export interface CompletionToken {
    access_token: string;
    /* expires in number of seconds since `created_at` */
    expires_in: number;
    /* unix timestamp of the datetime of token creation */
    created_at: number;
}

export class GitLabAPI implements IGitLabAPI {

    #completionToken: CompletionToken | undefined;
    #token: string;
    #baseURL: string;

    constructor(baseURL: string, token: string) {
        this.#token = token;
        this.#baseURL = baseURL;
    }

    async GetCodeSuggestions(file_info: CodeSuggestionRequestCurrentFile): Promise<CodeSuggestionResponse | undefined> {
        await this.#getCompletionToken();

        const request: CodeSuggestionRequest = {
            prompt_version: 1,
            project_path: "",
            project_id: -1,
            current_file: file_info
        };

        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.#completionToken?.access_token}`);
        headers.append('User-Agent', USER_AGENT);
        headers.append('Content-Type', 'application/json');
        headers.append('X-Gitlab-Authentication-Type', 'oidc');

        const response = await fetch(`https://codesuggestions.gitlab.com/v2/completions`, {
            method: 'POST',
            headers,
            body: JSON.stringify(request),
        } as RequestInit);

        let codeSuggResponse: CodeSuggestionResponse | undefined;
        const responseText = await response.text();

        try {
            codeSuggResponse = JSON.parse(responseText);
        } catch(e) {
            console.error(`Could not get code suggestion - status code = ${response.status}. Error: ${responseText}`);
        }
        
        return codeSuggResponse;
    }

    async #getCompletionToken(): Promise<string> {
        
        // Check if token has expired
        if (this.#completionToken) {
            const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
            if (unixTimestampNow < this.#completionToken.created_at + this.#completionToken.expires_in) {
                return this.#completionToken.access_token;
            }
        }

        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.#token}`);
        headers.append('User-Agent', USER_AGENT);
        const response = await fetch(`${this.#baseURL}/api/v4/code_suggestions/tokens`, {
            method: 'POST',
            headers,
        } as RequestInit);

        this.#completionToken = await response.json() as CompletionToken;
        return this.#completionToken.access_token;
    }
}