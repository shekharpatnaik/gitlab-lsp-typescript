import * as net from 'net';
import {
    Connection,
	TextDocuments,
	InitializeParams,
	CompletionItem,
	CompletionItemKind,
	TextDocumentPositionParams,
	InitializeResult,
} from 'vscode-languageserver';

import { createConnection } from 'vscode-languageserver/node';

import {
	TextDocument
} from 'vscode-languageserver-textdocument';
import { CodeSuggestionRequestCurrentFile, IGitLabAPI } from './api';

export enum Mode {
    STDIN,
    TCP
}

export class LspServer {

    #api: IGitLabAPI;
    #port: number;
    #documents: TextDocuments<TextDocument>;
    #mode: Mode;

    constructor(port: number, api: IGitLabAPI, mode: Mode) {
        this.#port = port;
        this.#api = api;
        this.#documents = new TextDocuments(TextDocument);
        this.#mode = mode;
    }

    createCodeCompletionParameters(textDocumentPosition: TextDocumentPositionParams): CodeSuggestionRequestCurrentFile {
        const doc = this.#documents.get(textDocumentPosition.textDocument.uri);
        const content_above_cursor = doc?.getText({
            start: {
                line: 0,
                character: 0
            },
            end: textDocumentPosition.position
        }) || '';

        const content_below_cursor = doc?.getText({
            start: textDocumentPosition.position,
            end: {
                line: doc.lineCount,
                character: -1,
            }
        }) || '';

        const fileUri = textDocumentPosition.textDocument.uri;
        const fileUriComponents = fileUri.split('/');
        const filename = fileUriComponents.length > 0 ? fileUriComponents[fileUriComponents.length - 1] : 'unknown';

        return {
            file_name: filename,
            content_above_cursor,
            content_below_cursor,
        };
    }

    #setupConnection(connection: Connection) {
        connection.onInitialize((_params: InitializeParams) => {
            const result: InitializeResult = {
                capabilities: {
                    completionProvider: {
                        resolveProvider: true
                    }
                }
            };
    
            return result;
        });
    
        // This handler provides the initial list of the completion items.
        connection.onCompletion(
            async (textDocumentPosition: TextDocumentPositionParams): Promise<CompletionItem[]> => {
                try {
                    const fileInfo = this.createCodeCompletionParameters(textDocumentPosition);
                    const suggestions = await this.#api.GetCodeSuggestions(fileInfo);
                    if (!suggestions) {
                        return [];
                    }
                    
                    return suggestions.choices.map((choice, index) => ({
                        label: `GitLab Suggestion ${index + 1}`,
                        kind: CompletionItemKind.Text,
                        insertText: choice.text,
                        data: index
                    } as CompletionItem));
                } catch(e) {
                    console.error(e);
                    return [];
                }
            }
        );
    
        connection.onCompletionResolve(
            (item: CompletionItem): CompletionItem => {
                return item;
            }
        );
    
        this.#documents.listen(connection);
    
        // Listen on the connection
        connection.listen();
    }

    start() {

        let connection: Connection;

        if (this.#mode == Mode.TCP) {
            const server = net.createServer((socket) => { 
                connection = createConnection(socket, socket);
                this.#setupConnection(connection);
            });
            server.listen(this.#port, '0.0.0.0', () => {
                console.log(`GitLab LSP started on port ${this.#port}`);
            });
        } else if (this.#mode == Mode.STDIN) {
            connection = createConnection(process.stdin, process.stdout);
            this.#setupConnection(connection); 
        } else {
            throw('Unsupported mode');
        }
    }
}